function guardarEnSessionStorage() {
  var codigoAlumno = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  var cursoAlumno = document.getElementById("idCurso").value; /* Referencia al input de clave */
  var txtValor = document.getElementById("idNota"); /* Referencia al input de valor */
  var clave = codigoAlumno.value + cursoAlumno;
  console.log(clave);
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  /*var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));*/
}
function leerDeSessionStorage() {
	
  var codigoAlumno = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  var cursoAlumno = document.getElementById("idCurso").value; /* Referencia al input de clave */
  var clave = codigoAlumno.value + cursoAlumno;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  console.log(valor);
  if (valor==null){
	  spanValor.innerText = "No existe nota";
  }else{
	    spanValor.innerText = valor;
  }

}

function removerEnSessionStorage() {
  var codigoAlumno = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  var cursoAlumno = document.getElementById("idCurso").value; /* Referencia al input de clave */
  var clave = codigoAlumno.value + cursoAlumno;
  sessionStorage.removeItem(clave);
}

function removerTodosEnSessionStorage() {
  sessionStorage.clear();
}
